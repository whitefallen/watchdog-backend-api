<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Objects\Inventory;
use Illuminate\Http\Request;
use DB;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function watchBlocks(Request $request) {
        $blockData = $request->get('inventory');
        $locationData = $request->get('location');
        if ($blockData && $locationData) {
            $newInv = new Inventory($blockData);
            $newBlock = Block::firstOrNew([
                'location' => $locationData
            ]);
            $newBlock->inventory = $newInv->inventory;
            $newBlock->save();
        }
        return '';
    }

    public function getAllBlocks(Request $request) {
        return Block::all();
    }
}
