<?php


namespace App\Objects;

/**
 * @property InventorySlot[] $inventory;
 * Class Inventory
 * @package App\Objects
 */
class Inventory
{
    public $inventory = [];

    public function __construct($_data)
    {
        foreach ($_data as $_slot) {
            $this->inventory[] = new InventorySlot($_slot);
        }
    }
}
