<?php


namespace App\Objects;

/**
 * @property int $v;
 * @property string $type;
 * @property int $amount;
 * Class InventorySlot
 * @package App\Objects
 */
class InventorySlot
{

    public $v;
    public $type;
    public $amount;

    /**
     * InventorySlot constructor.
     * @param mixed $_slot
     */
    public function __construct($_slot)
    {
        $this->v = $_slot['v'];
        $this->type = $_slot['type'];
        $this->amount = $_slot['amount'];
    }
}
