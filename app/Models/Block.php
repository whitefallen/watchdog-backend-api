<?php


namespace App\Models;
use App\Objects\Inventory;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class Block
 * @property Inventory[] inventory
 * @property String name
 * @package App\Models
 */
class Block extends Model
{
    protected $fillable = ['inventory','location'];
}
